Steps:
-----

1. Preprocess the training files

For spam classification:
Command: python3 preprocess.py -training -spam path/to/training/folder path/to/output/training/file
Example: python3 preprocess.py -training -spam SPAM_training spam_training.txt


For sentiment classification:
Command: python3 preprocess.py -training -sentiment path/to/training/folder path/to/output/training/file
Example: python3 preprocess.py -training -sentiment SENTIMENT_training sentiment_training.txt
-------------------------------------------------------------------------------------
2. Learn from the training data

Command: python3 nblearn.py spam_training.txt spam.nb
-------------------------------------------------------------------------------------
3. Preprocess the testing files

For spam classification:
Command: python3 preprocess.py -testing -spam path/to/testing/folder path/to/output/testing/file
Example: python3 preprocess.py -training -spam SPAM_dev spam_testing.txt

For sentiment classification:
Command: python3 preprocess.py -testing -sentiment path/to/testing/folder path/to/output/testing/file
Example: python3 preprocess.py -training -sentiment SENTIMENT_dev sentiment_testing.txt
-------------------------------------------------------------------------------------
4. Classify the testing files

Command: python3 nbclassify spam.nb spam_testing.txt
Or
python3 nbclassify spam.nb spam_testing.txt >> spam.out
-------------------------------------------------------------------------------------
-------------------------------------------------------------------------------------
preprocess Syntax:
Command: python3 preprocess.py -training|-testing -spam|-sentiment path/to/input/folder path/to/output/file
-------------------------------------------------------------------------------------

PART III
----------
*******SPAM-HAM CLASSIFIER******
SPAM:
Recall = 352/363 = 95.969%
Precision=352/362 = 97.513%
F Score=2PR/P+R = 0.9673

HAM:
Recall: 990/1000=99%
Precision=990/1001=98.9%
F Score=2PR/P+R= 0.9895

**********SENTIMENT ANALYSIS*********
POS:
Recall = 518/600 = 86.33
Precision = 518/563 = 92
F score = 0.89075

NEG:
Recall = 555/600 = 92.5
Precision = 555/637 = 87.127
F Score = 0.89733

SVM SPAM CLASSIFIER
*******************
precision =347/363 = 0.9559
recall = 347/386 = 0.8989
F score =0.9265


SVM SENTIMENT CLASSIFIER
*************************
POS:
Recall = 518/600 = 86.33
Precision = 518/563 = 92
F score = 0.89075


WHAT HAPPENS WITH 10 % DATA
******************************
With 10 percent training in SPAM(1800 records were used to train) the below values were obtained on dev classification
Recall = 325/363 = 89.5%
Precision = 325/332 =97.89 %
In general, the precision went higher because the records which were identified as SPAM  were indeed SPAM records. However, it could identify a very small portion in the total set because of which the recall values dropped drastically. This is because the learning chances of the classifier is limited as only limited number of words are seen in the bag of words features.So the misclassification rate is higher when there is limited training data
