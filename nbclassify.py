import sys,os,re
import ast,math
import profile

def main(argv):
	
	if len(sys.argv)!=3:
		print('Syntax is: python3 nbclassify.py /path/to/modelfile /path/to/testfile')
	model=open(sys.argv[1],'r')
	file=open(sys.argv[2],'r')
	priori,apriori=model.read().split('\n')
	findmostlikelyclass(file,priori,apriori)
	model.close()
	file.close()
	
#APRIORI EG: {'SPAM': {'abc':0.4,'def':0.4, 'ghi':0.2}, 'HAM': {'def':0.4,'ghi':0.4, 'abc':0.2}}
#PRIORI EG: {'SPAM' :0.5, 'HAM': 0.5}
	
def findmostlikelyclass(file,priori,apriori):
	aprioriforwords=dict()
	prioriclass=dict()
	apriori_training=ast.literal_eval(apriori)
	prioriclass=ast.literal_eval(priori)
	
	for line in file:
		words=line.replace('\n','').split(' ')
		likelyclass=dict()
		labelval=None
		label=''	
		for key, value in apriori_training.items():
			aprioriforwords[key]=0
			for word in words:
				try:
					aprioriforwords[key]+=math.log(value[word])
				except KeyError:
					keyerror=1	
		for key, value in prioriclass.items():
			likelyclass[key]=prioriclass[key]+aprioriforwords[key]
			if labelval==None:
				labelval=likelyclass[key]
				label=key
			elif labelval<likelyclass[key]:
				labelval=likelyclass[key]
				label=key
		print(label)
		
if __name__=="__main__":
	main(sys.argv[1:])
