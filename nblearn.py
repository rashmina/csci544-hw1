import sys,os,re
from collections import Counter

def main(argv):
	if len(sys.argv)!=3:
		print('Syntax is: python3 nblearn.py /path/to/spam_training.txt /path/to/spam.nb')
		sys.exit(2)
	
	## FIND LABELS, CLASSES AND VOCAB FROM THE FILE 
	## Eg:{'SPAM': 1, 'HAM':1}, {'SPAM':'abc def','HAM':'def ghi'}, {'abc', 'def', 'ghi'}
	labels,wordclasses=findlabels(sys.argv[1])
	vocab=set(' '.join(wordclasses.values()).split(' '))
	#print('found vocab')

	#diff=vocab.difference(set(wordclasses.values())
	#print(diff)

	## FIND PRIORIS FOR EACH SET OF LABELS Eg:P(SPAM), P(HAM) OR {'SPAM':0.5, 'HAM':0.5}
	priori=findpriori(labels)

	## FIND WORD FREQUENCY FOR EACH LABEL Eg:{'SPAM':{'abc':1 'def':1}, 'HAM':{'def':1,'ghi':1}}
	wordfrequency=findwordfrequency(wordclasses)
	#print(wordfrequency)

	##PERFORM SMOOTHING Eg:{'SPAM':{'abc':2 'def':2 'ghi':1}, 'HAM':{'def':2,'ghi':2, 'abc':1 }}
	## FIND APRIORIS AFTER SMOOTHING Eg:P('abc'|SPAM),P('def'|SPAM),P('ghi'|SPAM),P('def'|HAM) etc.
	## P('abc'|SPAM) = 1+1/len(SPAM)+len(vocab) = 2/2+3
	## P('ghi'|SPAM) = 0+1/2+3 etc
	## OR {'SPAM': {'abc':o.4,'def':0.4, 'ghi':0.2}, 'HAM': {'def':0.4,'ghi':0.4, 'abc':0.2}}
	apriori=findapriori(wordfrequency,vocab)
	#print(apriori)

	## WRITE PRIORIS AND APRIORIS TO A MODEL FILE
	file=open(sys.argv[2],'w')
	file.write(str(priori))
	file.write('\n')
	file.write(str(apriori))
	file.close()
	
	
	
def findlabels(path):
	labels=[]
	wordclasses=dict()
	file=open(sys.argv[1],'r')
	for line in file:
		newvocab=set()
		feature=[]
		
		feature=line.replace('\n',' ').split(None,1)[1:]
		featurestring=''.join(feature)
		if line.split(None,1)[0] not in labels:
			wordclasses[line.split(None,1)[0]]=featurestring
		else:
			wordclasses[line.split(None,1)[0]]+=featurestring
		labels.append(line.split(None,1)[0])
	file.close()
	return(dict(Counter(labels)),wordclasses)

def findpriori(labels):
	totalcount=sum(labels.values())
	for key, value in labels.items():
		labels[key]=(value/totalcount)
	return labels

def findwordfrequency(wordclasses):
	freq=dict()
	for key, value in wordclasses.items():
		freq[key]=dict(Counter(value.strip().split(' ')))
	return freq

def findapriori(frequency, vocab):
	k=len(vocab)
	labelfreq=dict()
	#print(frequency)
	#for word in vocab:
	for key, val in frequency.items():
		labelfreq[key]=sum(val.values())
		for word in val.keys():
			val[word]=(val[word]+1)/(labelfreq[key]+k)
		for word in vocab.difference(set(val.keys())):
			val[word]=1/(labelfreq[key]+k)

	return frequency

if __name__=="__main__":
	main(sys.argv[1:])


