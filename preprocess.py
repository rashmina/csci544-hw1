import sys,os,re

def main(argv):
	if len(sys.argv)!=5:
		print('Syntax is: python3 preprocess.py [-training|-testing] [-spam|-sentiment] /path/to/inputfolder /path/to/spam_training.txt')
		sys.exit(2)
	output=open(sys.argv[4],'w')
	dir=os.listdir(sys.argv[3])
	if sys.argv[2]=='-sentiment':
		for filename in sorted(dir):
			##print(file) 
			if sys.argv[1]=='-training':
				output.write(filename.split('.')[0]+' ')
			input=open(str(sys.argv[3]) + '/' + str(filename),'r',errors='ignore')
			## FIND REGEX PATTERNS WHICH ARE ONE OR MORE REPEATITIONS OF ALPHABETS
			##words=re.findall('[a-zA-Z][a-zA-Z]+',input.read())
			text=input.read().split(' ')
			words=ngrams(text,2)
			output.write(' '.join(words))
			word=ngrams(text,3)
			output.write(' '.join(words))
			output.write('\n')
	elif sys.argv[2]=='-spam':
		for filename in sorted(dir):
			##print(file) 
			if sys.argv[1]=='-training':
				output.write(filename.split('.')[0]+' ')
			input=open(str(sys.argv[3]) + '/' + str(filename),'r',errors='ignore')
			## FIND REGEX PATTERNS WHICH ARE ONE OR MORE REPEATITIONS OF ALPHABETS
			words=re.findall('[a-zA-Z][a-zA-Z]+',input.read().replace('Subject:',''))
			output.write(' '.join(words).lower())
			output.write('\n')
def ngrams(input,n):
	res=[]
	out=[]
	for i in range(len(input)-n+1):
		res.append(input[i:i+n])
	for x in res:
		out.append(' '.join(x))
	#print(out)	
	return out	
if __name__=="__main__":
	main(sys.argv[1:])
